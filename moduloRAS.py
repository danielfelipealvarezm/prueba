from os import strerror

def open_readme(name):
    print('\n')
    try:
        cnt = 0
        s = open('README.md', "rt")
        ch = s.read(1)
        while ch != '':
            print(ch, end='')
            if(cnt<=12):
                cnt += 1
            if(cnt == 12):
                print(name, end=' ')
            ch = s.read(1)
        s.close()
        print('\n')
    except IOError as e:
        print("Se produjo un error de E/S: ", strerror(e.errno))

def open_license(name):
    print('\n')
    print("Hola " + name + " la licencia de tu proyecto es:")
    stream = open("LICENSE", "rt", encoding = "utf-8") # se abre el archivo tzop.txt en modo lectura, devolviéndolo como un objeto de archivo
    print(stream.read()) # se imprime el contenido del archivo
    print('\n')

def welcome_to_git(name):
    selector = 0
    while(selector != 3):
        print("¡Bienvenido a Git! \n En este programa tienes dos funciones: \n \t1. Leer el README.md\n \
    \t 2. Conocer la licencia del proyecto\n \t 3. Salir")
        selector = int(input("¿Qué quieres realizar? "))
        if(selector == 1):
            open_readme(name)
        elif(selector == 2):
            open_license(name)
